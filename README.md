# bitcoin-confirmation-times-data
Jupyter notebook, processing Bitcoin Confirmation Times Data 

## Deployment
Clone or fork this repo and launch jupyter.

## Example
[Notebook viewer](http://nbviewer.jupyter.org/urls/gitlab.com/sgornick/bitcoin-confirmation-times-data/raw/master/FeeBreakdown.ipynb)

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
